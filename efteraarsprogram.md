Efterårsprogram 2021
=======
<style>
table, th, td {
    padding: 7px;
    margin-left: auto;
    margin-right: auto;
}
</style>

| Uge       | Biologi/Biotek                              | Fysik                                    |
|:----------|:--------------------------------------------|:-----------------------------------------|
| Uge 33-35 | 3d-print af setup (openScad)                | 3d-print af setup (openScad)             |
| Uge 36-38 | Opstilling klar med motorer/servoer         | Opstilling klar med motorer/servoer      |
| Uge 39-43 | Indføre og vedligeholde liv i akvariet      | Fange satellitsignal + SDR               |
| Uge 44-46 | Udstyre akvariet med måleudstyr og sensorer | Lave satellit der kan sende              |
| Uge 47    | Sende akvarie til vejrs med heliumballon    | Sende akvarie til vejrs med heliumballon |
