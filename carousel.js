/**
 * Generate a bootstrap carousel as described in https://getbootstrap.com/docs/5.0/components/carousel/
 *
 * Example:
 * generateCarousel("steamCampCarousel",  
       [{image: "images/steam-camps/3d-printer-2-1536x1031.jpg", caption: "3D printer", description: "Undervisning i design thinking"},
        {image: "images/steam-camps/em-field.jpg", caption: "Elektromagnetisme", description: "Tidlig bølgeforståelse"},
        {image: "images/steam-camps/sensoractuatorsdemo50pct.jpg", caption: "Italien 2019", description: "Arduino sensorer og funktionsbegrebet"}]
    );
 *
 */

var generateCarousel = function(targetDivId, carouselContent) {
    var carousel = document.getElementById(targetDivId);
    carousel.setAttribute("class", "carousel slide");
    carousel.setAttribute("data-bs-ride", "carousel");
    var carouselIndicators = document.createElement("div");
    carouselIndicators.setAttribute("class", "carousel-indicators");
    var carouselInner = document.createElement("div");
    carouselInner.setAttribute("class", "carousel-inner");
    carouselContent.forEach(function(carouselContentItem, i) {
        var button = document.createElement("button");
        button.setAttribute("data-bs-target", "#" + targetDivId);
        button.setAttribute("data-bs-slide-to", i);
        button.setAttribute("aria-label", "Slide " + i);
        if (i == 0) {
            button.setAttribute("class", "active");
        }
        carouselIndicators.appendChild(button);
        
        var carouselItem = document.createElement("div");
        carouselItem.setAttribute("class", i== 0? "carousel-item active" : "carousel-item");
        var image = document.createElement("img");
        image.setAttribute("class", "d-block w-100");
        image.setAttribute("src", carouselContentItem.image);
        carouselItem.appendChild(image);
        var itemCaption = document.createElement("div");
        itemCaption.setAttribute("class", "carousel-caption d-none d-md-block");
        itemCaption.innerHTML = "<h5>" + carouselContentItem.caption + "</h5>" + "<p>" + carouselContentItem.description + "</p>";
        carouselItem.appendChild(itemCaption);
        carouselInner.appendChild(carouselItem);
    });
    var prevButton = document.createElement("button");
    prevButton.setAttribute("class", "carousel-control-prev");
    prevButton.setAttribute("type", "button");
    prevButton.setAttribute("data-bs-target", "#" + targetDivId);
    prevButton.setAttribute("data-bs-slide", "prev");
    prevButtonSpan1 = document.createElement("span");
    prevButtonSpan1.setAttribute("class","carousel-control-prev-icon");
    prevButtonSpan1.setAttribute("aria-hidden", "true");
    prevButtonSpan2 = document.createElement("span");
    prevButtonSpan2.setAttribute("class","visually-hidden");
    prevButtonSpan2.innerHTML = "Prev";
    prevButton.appendChild(prevButtonSpan1);
    prevButton.appendChild(prevButtonSpan2);
    var nextButton = document.createElement("button");
    nextButton.setAttribute("class", "carousel-control-next");
    nextButton.setAttribute("type", "button");
    nextButton.setAttribute("data-bs-target", "#" + targetDivId);
    nextButton.setAttribute("data-bs-slide", "next");
    nextButtonSpan1 = document.createElement("span");
    nextButtonSpan1.setAttribute("class","carousel-control-next-icon");
    nextButtonSpan1.setAttribute("aria-hidden", "true");
    nextButtonSpan2 = document.createElement("span");
    nextButtonSpan2.setAttribute("class","visually-hidden");
    nextButtonSpan2.innerHTML = "Next";
    nextButton.appendChild(nextButtonSpan1);
    nextButton.appendChild(nextButtonSpan2);

    carousel.appendChild(carouselIndicators);
    carousel.appendChild(carouselInner);
    carousel.appendChild(prevButton);
    carousel.appendChild(nextButton);
}
