<div class="row">
    <div class="col mb-3 d-flex justify-content-center p-3 bg-primary">
        <h3>Wan to join? Find our next sessions <a href="https://www.facebook.com/rysteamcph/events/" class="link-light">here</a><h3>
    </div>
</div>
<div class="row">
    <div class="col mb-3 ml-5 p-0 bg-secondary">
        <div id="mainCarousel"></div>
    </div>
</div>
<div class="row">
    <div class="col mb-3 bg-info">
        <div class="row">
            <div class="col d-flex justify-content-center p-3">
            <h3>Follow us on:</h3>
            </div>
        </div>
        <div class="row">
            <div class="col d-flex justify-content-center p-3">
                <a href="https://www.instagram.com/rysteamcph/" class="link-light">
                    <img src="images/Instagram.svg" class="mx-auto d-block" width="50%">
                </a>
            </div>
            <div class="col d-flex justify-content-center p-3">
                <a href="https://www.facebook.com/rySteamCPH" class="link-light">
                    <img src="images/Facebook.svg" class="mx-auto d-block" width="20%">
                </a>
            </div>
            <div class="col d-flex justify-content-center p-3">
                <a href="https://gitlab.com/rysteam" class="link-light">
                    <img src="images/gitlablogo.svg" class="mx-auto d-block"  width="50%">
                </a>
            </div>
        </div>
    </div>
</div>
<style>
    .carousel-caption {
        background-color: #00000060;
    }
</style>
<script>
    generateCarousel("mainCarousel",
        [
            {image: "images/frontpage/team2020.jpg", caption: "The rÿSteam Team", description: "The 2020 team"},
            {image: "images/frontpage/3dprinting.jpg", caption: "Design thinking", description: "3D printing"},
            {image: "images/frontpage/coding+diy.jpg", caption: "Hands on!", description: "Learning by doing it!"},
        ]
    );
</script>
